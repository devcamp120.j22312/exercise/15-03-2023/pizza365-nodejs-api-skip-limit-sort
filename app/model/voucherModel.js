// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo drink Schema
const voucherSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId,
        //unique: true
    },
    maVoucher: {
        type: String,
        unique: true,
        required: true
    },
    phanTramGiamGia: {
        type: Number,
        required: true
    },
    ghiChu: {
        type: String,
        required: false
    }
}, {
    timestamps: true
})

// Biên dịch drink Model từ drinkSchema
module.exports = mongoose.model("Voucher", voucherSchema);
