// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo drink Schema
const userSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId,
        //unique: true
    },
    fullName: {
        type: String, 
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "Order"
    }]
}, {
    timestamps: true
})

// Biên dịch drink Model từ drinkSchema
module.exports = mongoose.model("User", userSchema);
