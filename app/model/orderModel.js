// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo drink Schema
const orderSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId
        // unique: true
    },
    orderCode: {
        type: String,
        default: true,
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "Voucher"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "Drink"
    },
    status: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

// Biên dịch drink Model từ drinkSchema
module.exports = mongoose.model("Order", orderSchema);
