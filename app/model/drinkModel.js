// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo drink Schema
const drinkSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId,
        require: true
    },
    maNuocUong: {
        type: String,
        unique: true,
        required: true
    },
    tenNuocUong: {
        type: String,
        required: true
    },
    donGia: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
})

// Biên dịch drink Model từ drinkSchema
module.exports = mongoose.model("Drink", drinkSchema);
