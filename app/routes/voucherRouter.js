// Khai báo thư viện express
const express = require('express');

// Import controller
const voucherController = require('../controllers/voucherController');

// Import Middleware
const voucherMiddleware = require('../middlewares/voucherMiddleware');

// Tạo router
const router = express.Router();

// Create drink
router.post("/vouchers", voucherMiddleware.createVoucherMiddleware, voucherController.createVoucher);

// Get all drink
router.get("/vouchers", voucherMiddleware.getAllVoucherMiddleware, voucherController.getAllVoucher);

// Get drink by id
router.get("/vouchers/:voucherId", voucherMiddleware.getDetailVoucherMiddleware, voucherController.getVoucherById);

// Update drink by id
router.put("/vouchers/:voucherId", voucherMiddleware.updateVoucherMiddleware, voucherController.updateVoucherById);

// Delete drink by Id
router.delete('/vouchers/:voucherId', voucherMiddleware.deleteVoucherMiddleware, voucherController.deleteVoucher);

module.exports = router;