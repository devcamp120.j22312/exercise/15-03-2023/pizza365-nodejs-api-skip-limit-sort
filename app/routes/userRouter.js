// Khai báo thư viện express
const express = require('express');

// Import user Controller
const userController = require('../controllers/userController');
const userMiddleware = require('../middlewares/userMiddleware');

// Import Middleware

// Tạo router
const router = express.Router();

// Create User
router.post("/users", userMiddleware.createUserMiddleware, userController.createUser);

// Get All User
router.get("/users", userMiddleware.getAllUserMiddleware, userController.getAllUser);

// Get User By Id
router.get("/users/:userId", userMiddleware.getDetailUserMiddleware, userController.getUserById);

// Update User By Id
router.put("/users/:userId", userMiddleware.updateUserMiddleware, userController.updateUserById);

// Delete User By Id
router.delete("/users/:userId", userMiddleware.deleteUserMiddleware, userController.deleteUserById);

module.exports = router;
