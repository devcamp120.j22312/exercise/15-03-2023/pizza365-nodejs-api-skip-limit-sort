// Khai báo thư viện express
const express = require('express');

// Import controller
const drinkController = require('../controllers/drinkController');

// Import Middleware
const drinkMiddleware = require('../middlewares/drinkMiddleware');

// Tạo router
const router = express.Router();

// Create drink
router.post("/drinks", drinkMiddleware.createDrinkMiddleware, drinkController.createDrink);

// Get all drink
router.get("/drinks", drinkMiddleware.getAllDrinkMiddleware, drinkController.getAllDrink);

// Get drink by id
router.get("/drinks/:drinkId", drinkMiddleware.getDetailDrinkMiddleware, drinkController.getDrinkById);

// Update drink by id
router.put("/drinks/:drinkId", drinkMiddleware.updateDrinkMiddleware, drinkController.updateDrinkById);

// Delete drink by Id
router.delete('/drinks/:drinkId', drinkMiddleware.deleteDrinkMiddleware, drinkController.deleteDrink);

module.exports = router;