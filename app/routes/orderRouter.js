// Khai báo thư viện mongoose
const express = require('express');

// Import order Controller
const orderController = require('../controllers/orderController');
const orderMiddleware = require('../middlewares/orderMiddleware');

// Tạo router
const router = express.Router();

// Create Order
router.post('/orders', orderMiddleware.createOrderMiddleware, orderController.createOrder);

// Get all order
router.get("/orders", orderMiddleware.getAllOrderMiddleware, orderController.getAllOrder);

// Get order by id 
router.get("/orders/:orderId", orderMiddleware.getDetailOrderMiddleware, orderController.getOrderById);

// Update Order By Id
router.put('/orders/:orderId', orderMiddleware.updateOrderMiddleware, orderController.updateOrderById);

// Delete order by id 
router.delete('/orders/:orderId', orderMiddleware.deleteOrderMiddleware, orderController.deleteOrderById);

module.exports = router;