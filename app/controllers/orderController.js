// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Import Order Model
const orderModel = require('../model/orderModel');

// Create Order
const createOrder = (request, response) => {
    // Thu thập dữ liệu
    let body = request.body;
    
    // Kiểm tra 
    if (!body.pizzaSize) {
        return response.status(400).json({
            message: 'Pizza Size is required!'
        })
    }
    if (!body.pizzaType) {
        return response.status(400).json({
            message: 'Pizza Type is required!'
        })
    }
    if (!body.status) {
        return response.status(400).json({
            message: 'Status is required!'
        })
    }

    // Xử lý và trả về kết quả
    let newOrder = ({
        _id: mongoose.Types.ObjectId(),
        orderCode: body.orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType

    })
    orderModel.create((error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            })
        }
        else {
            return response.status(201).json({
                message: 'Successfully created!',
                order: data
            })
        }
    })
}

// Get All Order 
const getAllOrder = (request, response) => {
    orderModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully load all data!',
                order: data
            })
        }
    })
}

// Get Order By Id
const getOrderById = (request, response) => {
    // Thu thập dữ liệu
    let orderId = request.params.orderId;

    // Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            message: "OrderId is invalid!"
        })
    }

    // Xử lý & trả về kết quả
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully get drink data!',
                order: data
            })
        }
    })
}

// Update Order By Id
const updateOrderById = (request, response) => {
    // Thu thập dữ liệu
    let orderId = request.params.orderId;
    let body = request.body;

    // Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            message: "DrinkId is invalid!"
        })
    }
    if (!body.pizzaSize) {
        return response.status(400).json({
            message: 'Pizza Size is required!'
        })
    }
    if (!body.pizzaType) {
        return response.status(400).json({
            message: 'Pizza Type is required!'
        })
    }
    if (!body.status) {
        return response.status(400).json({
            message: 'Status is required!'
        })
    }
    let updateOrder = {
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        status: body.status
    }
    orderModel.findByIdAndUpdate(orderId, updateOrder, (error, data) => {
        if (error) {
            return response.status(500).json({
            message: `Internal server error: ${error.message}`
        });
    } else {
        return response.status(201).json({
            message: 'Successfully updated!',
            order: data
            })
        }
    })
}

// Delete Order By Id
const deleteOrderById = (request, response) => {
    // Thu thập dữ liệu
    let orderId = request.params.orderId;

    // Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            message: 'DrinkId is invalid!'
        })
    }

    // Xử lý và trả về kết quả
    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if(error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            })
        }
        else {
            return response.status(200).json({
                message: 'Successfully deleted',
                order: data
            })
        }
    })
}

// Export thành module
module.exports = { 
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}