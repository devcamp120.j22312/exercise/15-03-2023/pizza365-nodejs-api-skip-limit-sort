// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Import Course Model
const drinkModel = require('../model/drinkModel');

// Function CRUD 

// Create Drink
const createDrink = (request, response) => {
    // Thu thập dữ liệu
    let body = request.body;

    // Kiểm tra
    if (!body.maNuocUong) {
        return response.status(400).json({
            message: 'Ma Nuoc Uong is required!'
        })
    }
    if (!body.tenNuocUong) {
        return response.status(400).json({
            message: 'Ten Nuoc Uong is required!'
        })
    }
    if (!body.donGia) {
        return response.status(400).json({
            message: 'Don Gia is required!'
        })
    }
    if (Number.isInteger(body.donGia) || body.donGia < 0) {
        return response.status(400).json({
            message: 'Don Gia is invalid!'
        })
    }
    
    // Xử lý và trả về kết quả
    let newDrink = new drinkModel ({
        _id : mongoose.Types.ObjectId(),
        maNuocUong : body.maNuocUong,
        tenNuocUong : body.tenNuocUong,
        donGia : body.donGia
    });

    drinkModel.create(newDrink, (error, data) => {
        if(error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully created!',
                drink: data
            })
        }
    })
}

// Get All Drink
const getAllDrink = (request, response) => {
    drinkModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully load all data!',
                course: data
            })
        }
    })
}

// Get Drink By Id 
const getDrinkById = (request, response) => {
    // Thu thập dữ liệu
    let drinkId = request.params.drinkId;
    
    // Kiểm tra 
    if (mongoose.Types.ObjectId.isValid(drinkId)) {
        return response.status(400).json({
            message: "DrinkId is invalid!"
        })
    }

    // Xử lý và trả về kết quả
    drinkModel.findById(drinkId, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully get drink data!',
                drink: data
            })
        }
    })
}

// Update Drink By Id
const updateDrinkById = (request, response) => {
    // Thu thập dữ liệu
    let drinkId = request.params.drinkId;
    let body = request.body;

    // Kiểm tra dữ liệu
    if (mongoose.Types.ObjectId.isValid(drinkId)) {
        return response.status(400).json({
            message: "DrinkId is invalid!"
        })
    }
    if (!body.maNuocUong) {
        return response.status(400).json({
            message: 'Ma Nuoc Uong is required!'
        })
    }
    if (!body.tenNuocUong) {
        return response.status(400).json({
            message: 'Ten Nuoc Uong is required!'
        })
    }
    if (!body.donGia) {
        return response.status(400).json({
            message: 'Don Gia is required!'
        })
    }
    if (Number.isInteger(body.donGia) || body.donGia < 0) {
        return response.status(400).json({
            message: 'Don Gia is invalid!'
        })
    }

    // Xử lý và trả về kết quả
    let updateDrink = {
        maNuocUong : body.maNuocUong,
        tenNuocUong : body.tenNuocUong,
        donGia : body.donGia
    }

    drinkModel.findByIdAndUpdate(drinkId, updateDrink, (error, data) => {
        if (error) {
            return response.status(500).json({
            message: `Internal server error: ${error.message}`
        });
    } else {
        return response.status(201).json({
            message: 'Successfully updated!',
            drink: data
            })
        }
    })
}

// Delete Drink
const deleteDrink = (request, response) => {
    // Thu thập dữ liệu
    let drinkId = request.params.drinkId;

    // Kiểm tra dữ liệu
    if (mongoose.Types.ObjectId.isValid(drinkId)) {
        return response.status(400).json({
            message: 'DrinkId is invalid!'
        })
    }

    // Xử lý và trả về kết quả 
    drinkModel.findByIdAndDelete(drinkId, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            })
        }
        else {
            return response.status(200).json({
                message: 'Successfully deleted!',
                drink: data
            })
        }
    })
}

// Export thành module
module.exports = {
    createDrink,
    getAllDrink,
    getDrinkById,
    updateDrinkById,
    deleteDrink
}