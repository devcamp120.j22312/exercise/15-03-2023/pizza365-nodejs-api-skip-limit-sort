// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Import Course Model
const voucherModel = require('../model/voucherModel');

// Function CRUD 

// Create Voucher
const createVoucher = (request, response) => {
    // Thu thập dữ liệu
    let body = request.body;

    // Kiểm tra
    if (!body.maVoucher) {
        return response.status(400).json({
            message: 'Ma Voucher is required!'
        })
    }
    if (!body.phanTramGiamGia) {
        return response.status(400).json({
            message: 'Phan Tram Giam Gia is required!'
        })
    }
    if (Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0) {
        return response.status(400).json({
            message: 'Phan Tram Giam Gia is invalid!'
        })
    }
    
    // Xử lý và trả về kết quả
    let newVoucher = ({
        _id : mongoose.Types.ObjectId(),
        maVoucher : body.maVoucher,
        phanTramGiamGia : body.phanTramGiamGia,
        ghiChu : body.ghiChu
    });

    voucherModel.create(newVoucher, (error, data) => {
        if(error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully created!',
                voucher: data
            })
        }
    })
}

// Get All Voucher
const getAllVoucher = (request, response) => {
    voucherModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully load all data!',
                voucher: data
            })
        }
    })
}

// Get Voucher By Id 
const getVoucherById = (request, response) => {
    // Thu thập dữ liệu
    let voucherId = request.params.voucherId;
    
    // Kiểm tra 
    if (mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            message: "VoucherId is invalid!"
        })
    }

    // Xử lý và trả về kết quả
    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully get voucher data!',
                voucher: data
            })
        }
    })
}

// Update Voucher By Id
const updateVoucherById = (request, response) => {
    // Thu thập dữ liệu
    let voucherId = request.params.voucherId;
    let body = request.body;

    // Kiểm tra dữ liệu
    if (mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            message: "voucherId is invalid!"
        })
    }
    if (!body.maVoucher) {
        return response.status(400).json({
            message: 'Ma Voucher is required!'
        })
    }
    if (!body.phanTramGiamGia) {
        return response.status(400).json({
            message: 'Phan Tram Giam Gia is required!'
        })
    }
    if (Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0) {
        return response.status(400).json({
            message: 'Phan Tram Giam Gia is invalid!'
        })
    }

    // Xử lý và trả về kết quả
    let updateVoucher = {
        maVoucher : body.maVoucher,
        phanTramGiamGia : body.phanTramGiamGia,
        ghiChu : body.ghiChu
    }

    voucherModel.findByIdAndUpdate(voucherId, updateVoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
            message: `Internal server error: ${error.message}`
        });
    } else {
        return response.status(201).json({
            message: 'Successfully updated!',
            voucher: data
            })
        }
    })
}

// Delete Voucher
const deleteVoucher = (request, response) => {
    // Thu thập dữ liệu
    let voucherId = request.params.voucherId;

    // Kiểm tra dữ liệu
    if (mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            message: 'voucherId is invalid!'
        })
    }

    // Xử lý và trả về kết quả 
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            })
        }
        else {
            return response.status(200).json({
                message: 'Successfully deleted!',
                voucher: data
            })
        }
    })
}

// Export thành module
module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucher
}