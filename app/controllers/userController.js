// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Import user Model
const userModel = require('../model/userModel');

// Create User
const createUser = (request, response) => {
    const body = request.body;

    // Kiểm tra dữ liệu
    if (!body.fullName) {
        return response.status(400).json({
            message: 'Full name is required!'
        })
    }
    if (!body.email) {
        return response.status(400).json({
            message: 'Email is required!'
        })
    }
    if (!body.address) {
        return response.status(400).json({
            message: 'Address is required!'
        })
    }
    if (!body.phone) {
        return response.status(400).json({
            message: 'Phone is required!'
        })
    }

    // Xử lý và trả về kết quả
    const newUser = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone,
    }

    userModel.create(newUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully created!',
                data: data
            })
        }
    })
}

// Get All User
const getAllUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all user successfully",
            data: data
        })
    })
}

// Get User By Id
const getUserById = (request, response) => {
    // Thu thập dữ liệu
    let userId = request.params.userId;

    // Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "User ID is not valid!"
        })
    }

    // Xử lý dữ liệu & trả về kết quả
    userModel.findById(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully get user data!',
                data: data
            })
        }
    })
}

// update user by id
const updateUserById = (request, response) => {
    const userId = request.params.userId;
    const body = request.body;

    // Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "UserId không hợp lệ"
        })
    }
    if (!body.fullName) {
        return response.status(400).json({
            message: 'Full name is required!'
        })
    }
    if (!body.email) {
        return response.status(400).json({
            message: 'Email is required!'
        })
    }
    if (!body.address) {
        return response.status(400).json({
            message: 'Address is required!'
        })
    }
    if (!body.phone) {
        return response.status(400).json({
            message: 'Phone is required!'
        })
    }
    // Xử lý và trả về kết quả
    const updateUser = {}
    if (body.fullName !== undefined) {
        updateUser.fullName = body.fullName
    }
    if (body.email !== undefined) {
        updateUser.email = body.email
    }
    if (body.address !== undefined) {
        updateUser.address = body.address
    }
    if (body.phone !== undefined) {
        updateUser.phone = body.phone
    }

    userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully updated!',
                data: data
            })
        }
    })
}

// Delete user
const deleteUserById = (request, response) => {
    // Thu thập dữ liệu
    let userId = request.params.userId;

    // Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            message: 'userId is invalid!'
        })
    }

    // Xử lý và trả ra kết quả
    userModel.findByIdAndDelete(userId, (error) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.mesage}`
            })
        }
        else {
            return response.status(200).json({
                message: 'Successfully deleted!',
            })
        }
    })
}

// Get all user limit
const getAllUserLimit = (request, response) => {
    let limitNumber = request.query.limitNumber;

    // Nếu sử dụng limit tại url /api/limit-user?limitNumber=(number)
    if (limitNumber) {
        userModel.find()
            .limit(limitNumber)
            .exec((error, data) => {
                if (error) {
                    return response.status(500).json({
                        status: 'Internal server error',
                        message: error.message
                    })
                }
                else {
                    return response.status(200).json({
                        status: 'Get limited user success',
                        data: data
                    })
                }
            })
    }
    else {
        userModel.find((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            }
            return response.status(200).json({
                status: 'Get All users success',
                data: data
            })
        })
    }
}

// Get All User Skip
const getAllUserSkip = (request, response) => {
    const skipNumber = request.query.skipNumber;
    if (skipNumber) {
        userModel.find()
            .skip(skipNumber)
            .exec((error, data) => {
                if (error) {
                    return response.status(500).json({
                        status: 'Internal server error',
                        message: error.message
                    })
                }
                else {
                    return response.status(200).json({
                        status: 'Success: Get skipped users',
                        data: data
                    })
                }
            })
    }
    else {
        userModel.find((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            }
            else {
                return response.status(200).json({
                    status: 'Success: Get All users success',
                    data: data
                })
            }
        })
    }
}

// sort user by ABC
const sortUserByABC = (request, response) => {
    userModel.find()
        .sort({ fullName: "asc" })
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            }
            else {
                return response.status(200).json({
                    status: 'Success: Get users by alphabet success',
                    data: data
                })
            }
        })
}

// Skip + Limit Get User
const getAllUserSkipLimit = (request, response) => {
    const skipNumber = request.query.skipNumber;
    if (skipNumber) {
        userModel.find()
            .skip(skipNumber)
            .limit(1)
            .exec((error, data) => {
                if (error) {
                    return response.status(500).json({
                        status: 'Internal server error',
                        message: error.message
                    })
                }
                else {
                    return response.status(200).json({
                        status: 'Success: Get skip limit',
                        data: data
                    })
                }
            })
    }
    else {
        userModel.find((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            }
            else {
                return response.status(200).json({
                    status: 'Success: Get All users success',
                    data: data
                })
            }
        })
    }
}

// Function lấy user đầu tiên từ vị trí skip sau khi sort được alphabet
const sortSkipLimitUser = (request, response) => {
    const skipNumber = request.query.skipNumber;

    userModel.find()
        .sort({ fullName: "asc" })
        .skip(skipNumber)
        .limit(1)
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            }
            else {
                return response.status(200).json({
                    status: 'Success: Get skip limit users success',
                    data: data
                })
            }
        })
}

// Export thành module
module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
    getAllUserLimit,
    getAllUserSkip,
    sortUserByABC,
    getAllUserSkipLimit,
    sortSkipLimitUser
}